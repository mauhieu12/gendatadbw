# Reading an excel file using Python
import random

import pandas as pd
import xlsxwriter

HAVE_CER_CHOICE = [True, True, False]
CERTIFICATE_LIST = ['IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS', 'IELTS',
                    'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEIC', 'TOEFL', 'CPE']

if __name__ == '__main__':
    data = pd.read_excel('02.xlsx')
    df = pd.DataFrame(data, columns=['SBD'])

    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook('Student Certificate.xlsx')
    worksheet = workbook.add_worksheet(name='sheet_name')
    worksheet.write('A1', 'SBD')
    worksheet.write('B1', 'Loại chứng chỉ')
    worksheet.write('C1', 'Điểm số')

    current_row = 2
    for data in df.values:
        worksheet.write('A' + current_row.__str__(), data)
        if (random.choice(HAVE_CER_CHOICE) and data.__str__() != '[]'):
            point = random.randrange(300, 900, 1) / 10
            worksheet.write('C' + current_row.__str__(), point)

            cer_name = random.choice(CERTIFICATE_LIST)
            worksheet.write('B' + current_row.__str__(), cer_name)
        current_row += 1

    workbook.close()
